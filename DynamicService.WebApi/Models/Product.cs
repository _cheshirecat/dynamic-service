﻿namespace DynamicService.WebApi.Models
{
    using System.Collections.Generic;

    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public decimal Price { get; set; }

        public List<Detail> Details { get; set; }
    }

    public class Detail
    {
        public string Name { get; set; }

        public string Brand { get; set; }
    }
}