﻿using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;

namespace DynamicService.Client.App_Start
{
    using System.Web.Mvc;
    using Models.Repositories;

    public class SimpleInjectorConfig
    {
        public static void Initialize()
        {
            var container = new Container();

            container.Register<IProductRepository, ProductRepository>(Lifestyle.Transient);
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}