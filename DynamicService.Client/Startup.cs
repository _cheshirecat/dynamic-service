﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DynamicService.Client.Startup))]
namespace DynamicService.Client
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
