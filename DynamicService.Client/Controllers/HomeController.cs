﻿namespace DynamicService.Client.Controllers
{
    using Models.Repositories;
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        private IProductRepository Repository { get; set; }

        public HomeController(IProductRepository repository)
        {
            Repository = repository;
        }

        public ActionResult Index()
        {
            return View(Repository.GetProducts());
        }
    }
}