﻿namespace DynamicService.Client.Models.Repositories
{
    public interface IProductRepository
    {
        dynamic GetProducts();
    }
}