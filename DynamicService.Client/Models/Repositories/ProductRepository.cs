﻿using DynamicService.Configuration;

namespace DynamicService.Client.Models.Repositories
{
    public class ProductRepository : IProductRepository
    {
        public dynamic GetProducts()
        {
            var settings = new Settings("http://localhost:9810/api/products");
            var dynamicService = new DynamicService(settings);
            var serviceData = dynamicService.Dynamic;

            return serviceData;
        }
    }
}