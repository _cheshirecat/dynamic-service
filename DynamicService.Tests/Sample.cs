﻿namespace DynamicService.Tests
{
    internal static class Sample
    {
        internal static string Read(string folderName, string sampleName)
        {
            return System.IO.File.ReadAllText(string.Format(
                "{0}\\Converters\\{1}\\{2}", System.IO.Directory.GetCurrentDirectory(), folderName, sampleName));
        }
    }
}