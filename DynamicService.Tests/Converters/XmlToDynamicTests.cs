﻿using DynamicService.Configuration;
using DynamicService.Converters;
using DynamicService.RawData;

namespace DynamicService.Tests.Converters
{
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class XmlToDynamicTests
    {
        [Test]
        public void CdCatalog()
        {
            // let's arrange
            var rawData = Sample.Read("Xml Samples", "CD Catalog.xml");
            var serviceRawData = new ServiceRawData(rawData);
            var converter = new XmlToDynamic(serviceRawData, InternalSettings.PropertyNamingRules);

            // act and assert
            dynamic cds = converter.Dynamic.CATALOG.CD;

            string titlePlus = "Album: " + cds[0].TITLE;
            titlePlus.Should().Be("Album: Empire Burlesque");

            string lastArtist = "Last artist: " + cds[cds.Count - 1].ARTIST;
            lastArtist.Should().Be("Last artist: Joe Cocker");

            ((string)cds[cds.Count - 1].PRICE).Should().Be("8.20");
        }

        [Test]
        public void WearCatalog()
        {
            // let's arrange
            var rawData = Sample.Read("Xml Samples", "Wear Catalog.xml");
            var serviceRawData = new ServiceRawData(rawData);
            var extractor = new XmlToDynamic(serviceRawData, InternalSettings.PropertyNamingRules);

            // act and assert
            string croco = "crocodiles " + extractor.Dynamic.catalog.product.catalog_item[0].price;
            croco.Should().Be("crocodiles 39.95");

            string gender = extractor.Dynamic.catalog.product.catalog_item[1].gender;
            gender.Should().Be("Women's");

            string price = extractor.Dynamic.catalog.product.catalog_item[1].price;
            price.Should().Be("42.50");

            string yellow = extractor.Dynamic.catalog.product.catalog_item[1].size[2].color_swatch[2];
            yellow.Should().Be("Yellow");

            // TODO: it ougth to work
            //string medium = extractor.Dynamic.catalog.product.catalog_item[1].size[1].description;
            //medium.Should().Be("Medium");
        }

        [Test]
        public void Youtube()
        {
            // let's arrange
            var rawData = Sample.Read("Xml Samples", "YouTube.xml");
            var serviceRawData = new ServiceRawData(rawData);
            var extractor = new XmlToDynamic(serviceRawData, InternalSettings.PropertyNamingRules);

            // act && assert
            string apiVersion = extractor.Dynamic.ytData.apiVersion;
            apiVersion.Should().Be("2.0");

            string totalItems = extractor.Dynamic.ytData.data.totalItems;
            totalItems.Should().Be(800.ToString());

            string id = extractor.Dynamic.ytData.data.items.id;
            id.Should().Be("hYB0mn5zh2c");

            string tag1 = extractor.Dynamic.ytData.data.items.tags[0];
            tag1.Should().Be("GDD07");
            string tag3 = extractor.Dynamic.ytData.data.items.tags[2];
            tag3.Should().Be("Maps");

            //
            string dt = extractor.Dynamic.ytData.data.items.thumbnail.defaultDS;
            dt.Should().Be("http://i.ytimg.com/vi/hYB0mn5zh2c/default.jpg");

            string content1 = extractor.Dynamic.ytData.data.items.content.c1;
            content1.Should().Be("rtsp://v5.cache3.c.youtube.com/CiILENy.../0/0/0/video.3gp");
            //

            string videoRespond = extractor.Dynamic.ytData.data.items.accessControl.videoRespond;
            videoRespond.Should().Be("moderated");
        }
    }
}
