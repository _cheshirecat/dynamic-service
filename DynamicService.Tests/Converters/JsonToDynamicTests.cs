﻿using DynamicService.Configuration.PropertyNaming;
using DynamicService.Configuration.PropertyNaming.BenchmarkData;
using DynamicService.Converters;
using DynamicService.RawData;

namespace DynamicService.Tests.Converters
{
    using FluentAssertions;
    using NUnit.Framework;
    using System.Collections.Generic;

    [TestFixture]
    public class JsonToDynamicTests
    {
        [Test]
        public void Foo()
        {
            // let's arrange
            var a = 0;

            // act
            a = a + 5;

            // assert
            a.Should().Be(5);
        }

        [Test]
        public void Colors()
        {
            // let's arrange
            var rawData = Sample.Read("Json Samples", "Colors.json");
            var serviceRawData = new ServiceRawData(rawData);
            var converter = new JsonToDynamic(serviceRawData, Configuration.InternalSettings.PropertyNamingRules);

            // act
            var result = converter.Dynamic;

            // assert
            ((string)result.colorsArray[0].colorName).Should().Be("red");

            string redGreen = "green" + result.colorsArray[0].colorName;
            redGreen.Should().Be("greenred");

            ((string)result.colorsArray[0].hexValue).Should().Be("#f00");
            ((string)result.colorsArray[5].colorName).Should().Be("yellow");
            ((string)result.colorsArray[6].hexValue).Should().Be("#000");
        }

        [Test]
        public void Twitter()
        {
            // let's arrange
            var rawData = Sample.Read("Json Samples", "Twitter.json");
            var serviceRawData = new ServiceRawData(rawData);
            var converter = new JsonToDynamic(serviceRawData, Configuration.InternalSettings.PropertyNamingRules);

            // act
            var dynamicServiceData = converter.Dynamic;

            // assert
            var results = (List<object>)dynamicServiceData.results;
            results.Should().NotBeNull();

            dynamic r0 = results[0];
            ((string)(r0.text)).Should().Be("@twitterapi  http://tinyurl.com/ctrefg");

            bool zero = (dynamicServiceData.since_id == 0);
            zero.Should().BeTrue();
            int i = 11 + dynamicServiceData.since_id + 2;
            i.Should().Be(13);

            dynamic r1 = results[1];
            ((string)r1.brand).Should().Be("Stone Island");
            ((bool)r1.flag).Should().BeFalse();

            ((int)dynamicServiceData.since_id).Should().Be(0);
            ((int)dynamicServiceData.max_id).Should().Be(1480307926);
            ((string)dynamicServiceData.refresh_url).Should().Be("?since_id=1480307926&q=%40twitterapi");
            ((int)dynamicServiceData.results_per_page).Should().Be(15);

            decimal d = dynamicServiceData.completed_in * (decimal)0.100095;
            d.Should().Be((decimal)0.00317341188);
            ((double)dynamicServiceData.completed_in).Should().Be(0.031704);

            ((int)dynamicServiceData.page).Should().Be(1);

            string s = "this is: " + dynamicServiceData.query;
            s.Should().Be("this is: %40twitterapi");
            ((string)dynamicServiceData.query).Should().Be("%40twitterapi");
        }

        [Test]
        public void Youtube()
        {
            // let's arrange
            var rawData = Sample.Read("Json Samples", "YouTube.json");
            var serviceRawData = new ServiceRawData(rawData);
            var extractor = new JsonToDynamic(serviceRawData, Configuration.InternalSettings.PropertyNamingRules);

            // act && assert
            string apiVersion = extractor.Dynamic.apiVersion;
            apiVersion.Should().Be("2.0");

            int totalItems = extractor.Dynamic.data.totalItems;
            totalItems.Should().Be(800);

            string id = extractor.Dynamic.data.items[0].id;
            id.Should().Be("hYB0mn5zh2c");

            string tag1 = extractor.Dynamic.data.items[0].tags[0];
            tag1.Should().Be("GDD07");
            string tag3 = extractor.Dynamic.data.items[0].tags[2];
            tag3.Should().Be("Maps");

            string dt = extractor.Dynamic.data.items[0].thumbnail.defaultDS;
            dt.Should().Be("http://i.ytimg.com/vi/hYB0mn5zh2c/default.jpg");

            string content1 = extractor.Dynamic.data.items[0].content.DS1;
            content1.Should().Be("rtsp://v5.cache3.c.youtube.com/CiILENy.../0/0/0/video.3gp");
            string content7 = extractor.Dynamic.data.items[0].content.DS7;
            content7.Should().Be("www.seven.com");

            string videoRespond = extractor.Dynamic.data.items[0].accessControl.videoRespond;
            videoRespond.Should().Be("moderated");
        }

        [Test]
        public void MayPhone()
        {
            // let'a arrange
            var rawData = Sample.Read("Json Samples", "iPhone.json");
            var serviceRawData = new ServiceRawData(rawData);
            var extractor = new JsonToDynamic(serviceRawData, Configuration.InternalSettings.PropertyNamingRules);

            // act && asserts
            ((string)extractor.Dynamic.menu.header).Should().Be("xProgress SVG Viewer");
            ((string)extractor.Dynamic.menu.items[0].id).Should().Be("Open");
            ((string)extractor.Dynamic.menu.items[1].id).Should().Be("OpenNew");
            ((string)extractor.Dynamic.menu.items[1].label).Should().Be("Open New");
            ((object)extractor.Dynamic.menu.items[2]).Should().Be(null);
        }

        [Test]
        public void CustomNameValidator()
        {
            // let's arrange
            var rawData = Sample.Read("Json Samples", "YouTube (Custom name validator).json");
            var rules = new List<PropertyNamingRule>
            {
                new PropertyNamingRule(new NameParts("case", "CustomPrefix"), WhatDoWithParts.ToBegining),
                new PropertyNamingRule(new NameParts("string", "customprefix"), WhatDoWithParts.ToBegining)
            };
            var serviceRawData = new ServiceRawData(rawData);
            var extractor = new JsonToDynamic(serviceRawData, rules);

            // let's act and arrange this code
            var d = extractor.Dynamic;

            string caseStr = extractor.Dynamic.data.items[0].content.CustomPrefixcase;
            caseStr.Should().Be("this is custom case property");

            string stringStr = extractor.Dynamic.data.items[0].content.customprefixstring;
            stringStr.Should().Be("this is string property");
        }
    }
}