﻿namespace DynamicService
{
    using Configuration;
    using Configuration.PropertyNaming;
    using Converters;
    using RawData;
    using System.Collections.Generic;

    public class DynamicService
    {
        public DynamicService(Settings settings)
        {
            Settings = settings;
        }

        private Settings Settings { get; set; }

        public dynamic Dynamic
        {
            get
            {
                var rawData = GetRawData();
                var rules = GetRules();
                var converter = ConverterFactory.Converter(rawData, rules);
                var dynamic = converter.Dynamic;

                return dynamic;
            }
        }

        protected virtual RawData.RawData GetRawData()
        {
            var provider = RawDataProviderFactory.Provider(Settings.DataAddress);
            return provider.RawData;
        }

        protected virtual List<PropertyNamingRule> GetRules()
        {
            return Settings.PropertyNamingRule;
        }
    }
}