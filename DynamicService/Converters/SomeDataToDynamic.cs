﻿namespace DynamicService.Converters
{
    using Configuration.PropertyNaming;
    using RawData;
    using System.Collections.Generic;

    internal abstract class SomeDataToDynamic
    {
        private dynamic _innerDynamic;

        protected SomeDataToDynamic(RawData rawData, List<PropertyNamingRule> namingRules)
        {
            RawData = rawData;
            NamingRules = namingRules;
        }

        internal dynamic Dynamic
        {
            get { return _innerDynamic ?? (_innerDynamic = GetDynamicImpl()); }
        }

        protected RawData RawData { get; private set; }

        protected List<PropertyNamingRule> NamingRules { get; private set; }

        protected abstract dynamic GetDynamicImpl();
    }
}