﻿namespace DynamicService.Converters
{
    using Configuration.PropertyNaming;
    using RawData;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Web.Script.Serialization;

    internal class JsonToDynamic : SomeDataToDynamic
    {
        public JsonToDynamic(RawData rawData, List<PropertyNamingRule> namingRules)
            : base(rawData, namingRules)
        {
        }

        protected override dynamic GetDynamicImpl()
        {
            var serializer = new JavaScriptSerializer();
            var raw = serializer.Deserialize<object>(RawData.Raw.ToString());
            var dictionary = PrepareDictionary(raw);

            return ToDynamic(dictionary);
        }

        private IDictionary<string, object> PrepareDictionary(object raw)
        {
            var dictionary = raw as Dictionary<string, object>;
            if (dictionary != null)
            {
                return dictionary;
            }

            var array = raw as Array;
            if (array == null)
            {
                return null;
            }

            //var i = 0;
            //const string arrayItemName = "item";
            dictionary = new Dictionary<string, object>
            {
                { "Items", array }
            };
            //foreach (var item in array)
            //{
            //    dictionary.Add(arrayItemName + i, item);
            //    ++i;
            //}
            return dictionary;
        }

        private ExpandoObject ToDynamic(IDictionary<string, object> dictionary)
        {
            var expando = new ExpandoObject();
            var expandoDic = (IDictionary<string, object>)expando;

            // go through the items in the dictionary and copy over the key value pairs)
            foreach (var kvp in dictionary)
            {
                // if the value can also be turned into an ExpandoObject, then do it!
                if (kvp.Value is IDictionary<string, object>)
                {
                    var expandoValue = ToDynamic((IDictionary<string, object>)kvp.Value);

                    expandoDic.Extend(kvp.Key, expandoValue, NamingRules);
                }
                else if (kvp.Value is ICollection)
                {
                    // iterate through the collection and convert any strin-object dictionaries
                    // along the way into expando objects
                    var itemList = new List<object>();
                    foreach (var item in (ICollection)kvp.Value)
                    {
                        if (item is IDictionary<string, object>)
                        {
                            var expandoItem = ToDynamic((IDictionary<string, object>)item);
                            itemList.Add(expandoItem);
                        }
                        else
                        {
                            itemList.Add(item);
                        }
                    }

                    expandoDic.Extend(kvp.Key, itemList, NamingRules);
                }
                else
                {
                    expandoDic.Extend(kvp, NamingRules);
                }
            }

            return expando;
        }
    }
}