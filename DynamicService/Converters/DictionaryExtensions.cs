﻿namespace DynamicService.Converters
{
    using Configuration.PropertyNaming;
    using System.Collections.Generic;

    internal static class DictionaryExtensions
    {
        public static void Extend(this IDictionary<string, object> dictionary, KeyValuePair<string, object> item, List<PropertyNamingRule> rules)
        {
            dictionary.Extend(item.Key, item.Value, rules);
        }

        public static void Extend(this IDictionary<string, object> dictionary, string key, object value, List<PropertyNamingRule> rules)
        {
            PropertyNameValidator.BrushPropertyName(rules, ref key);

            dictionary.Add(key, value);
        }
    }
}