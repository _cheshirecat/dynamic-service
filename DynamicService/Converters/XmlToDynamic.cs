﻿namespace DynamicService.Converters
{
    using Configuration.PropertyNaming;
    using RawData;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq;
    using System.Xml.Linq;

    internal class XmlToDynamic : SomeDataToDynamic
    {
        public XmlToDynamic(RawData rawData, List<PropertyNamingRule> namingRules)
            : base(rawData, namingRules)
        {
        }

        protected override dynamic GetDynamicImpl()
        {
            var xDoc = XDocument.Parse(RawData.Raw.ToString());
            dynamic result = new ExpandoObject();
            Parse(result, xDoc.Elements().First());

            return result;
        }

        private void Parse(dynamic parent, XElement node)
        {
            if (node.HasElements)
            {
                if (node.Elements(node.Elements().First().Name.LocalName).Count() > 1)
                {
                    //list
                    var item = new ExpandoObject();
                    var list = new List<dynamic>();
                    foreach (var element in node.Elements())
                    {
                        Parse(list, element);
                    }

                    ProcessProperty(item, node.Elements().First().Name.LocalName, list);
                    ProcessProperty(parent, node.Name.ToString(), item);
                }
                else
                {
                    var item = new ExpandoObject();

                    foreach (var attribute in node.Attributes())
                    {
                        ProcessProperty(item, attribute.Name.ToString(), attribute.Value.Trim());
                    }

                    //element
                    foreach (var element in node.Elements())
                    {
                        Parse(item, element);
                    }

                    ProcessProperty(parent, node.Name.ToString(), item);
                }
            }
            else
            {
                ProcessProperty(parent, node.Name.ToString(), node.Value.Trim());
            }
        }

        private void ProcessProperty(dynamic parent, string name, object value)
        {
            PropertyNameValidator.BrushPropertyName(NamingRules, ref name);

            var parentAsList = parent as List<dynamic>;
            if (parentAsList != null)
            {
                parentAsList.Add(value);
                return;
            }

            var parentAsDictionary = parent as IDictionary<string, object>;
            if (parentAsDictionary != null)
            {
                if (parentAsDictionary.ContainsKey(name))
                {
                    ConvertExistingPropertyToList(parentAsDictionary, name, value);
                    return;
                }

                parentAsDictionary[name] = value;
            }
        }

        private void ConvertExistingPropertyToList(IDictionary<string, object> parentAsDictionary, string existingPropertyName, object value)
        {
            if (parentAsDictionary[existingPropertyName] is List<dynamic>)
            {
                ((List<dynamic>)parentAsDictionary[existingPropertyName]).Add(value);
                return;
            }

            var existingProperty = parentAsDictionary[existingPropertyName];
            parentAsDictionary[existingPropertyName] = new List<dynamic>();

            ((List<dynamic>)parentAsDictionary[existingPropertyName]).Add(existingProperty);
            ((List<dynamic>)parentAsDictionary[existingPropertyName]).Add(value);
        }
    }
}