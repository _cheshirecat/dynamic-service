﻿namespace DynamicService.Converters
{
    using Configuration.PropertyNaming;
    using System.Collections.Generic;
    using RawData;

    internal static class ConverterFactory
    {
        internal static SomeDataToDynamic Converter(RawData rawData, List<PropertyNamingRule> rules)
        {
            var type = GetResponseType(rawData);

            switch (type)
            {
                case "xml":
                    return new XmlToDynamic(rawData, rules);
                case "json":
                    return new JsonToDynamic(rawData, rules);
                default:
                    throw new System.Exception();
            }
        }

        internal static string GetResponseType(RawData rawData)
        {
            var firstChar = rawData.Raw.ToString().Trim()[0];

            switch (firstChar)
            {
                case '<':
                    return "xml";
                case '{':
                case '[':
                    return "json";
                default:
                    return string.Empty;
            }
        }
    }
}