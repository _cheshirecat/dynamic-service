﻿namespace DynamicService.RawData
{
    internal abstract class DataAddress
    {
        internal DataAddress(object address)
        {
            Address = address;
        }

        public object Address { get; private set; }
    }
}