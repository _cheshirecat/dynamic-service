namespace DynamicService.RawData
{
    internal enum AddressType
    {
        ServiceUrl,
        DbConnectionString
    }
}