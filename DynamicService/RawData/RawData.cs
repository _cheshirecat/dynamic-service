﻿namespace DynamicService.RawData
{
    public abstract class RawData
    {
        internal RawData(object raw)
        {
            Raw = raw;
        }

        internal object Raw { get; private set; }
    }
}