﻿namespace DynamicService.RawData
{
    using System;

    internal static class RawDataProviderFactory
    {
        internal static RawDataProvider<DataAddress, RawData> Provider(string dataAddress)
        {
            var addressType = GetAddressType(dataAddress);

            switch (addressType)
            {
                case AddressType.ServiceUrl:
                    return new ServiceRawDataProvider(dataAddress);
                case AddressType.DbConnectionString:
                    throw new Exception();
                default:
                    throw new Exception();
            }
        }

        private static AddressType GetAddressType(string dataAddress)
        {
            if (IsUrl(dataAddress))
            {
                return AddressType.ServiceUrl;
            }

            throw new Exception();
        }

        private static bool IsUrl(string dataAddress)
        {
            Uri uriResult;
            var isUrl = Uri.TryCreate(dataAddress, UriKind.Absolute, out uriResult)
                        && ((uriResult.Scheme == Uri.UriSchemeHttp)
                            || (uriResult.Scheme == Uri.UriSchemeHttps));

            return isUrl;
        }
    }
}