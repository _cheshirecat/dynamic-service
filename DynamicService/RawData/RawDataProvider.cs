﻿namespace DynamicService.RawData
{
    internal abstract class RawDataProvider<TDataAddress, TRawData>
    {
        internal RawDataProvider(string dataAddress)
        {
            DataAddress = dataAddress;
        }

        protected string DataAddress { get; set; }

        public TRawData RawData
        {
            get
            {
                var dataAddress = GetDataAddress();
                return GetRawData(dataAddress);
            }
        }

        internal abstract TDataAddress GetDataAddress();

        internal abstract TRawData GetRawData(TDataAddress dataAddress);
    }
}