﻿namespace DynamicService.RawData
{
    internal class ServiceDataAddress : DataAddress
    {
        public ServiceDataAddress(object address)
            : base(address)
        {
        }
    }
}