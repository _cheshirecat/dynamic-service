﻿namespace DynamicService.RawData
{
    using System.Net;

    internal class ServiceRawDataProvider : RawDataProvider<DataAddress, RawData>
    {
        public ServiceRawDataProvider(string dataAddress)
            : base(dataAddress)
        {
        }

        internal override DataAddress GetDataAddress()
        {
            return new ServiceDataAddress(DataAddress);
        }

        internal override RawData GetRawData(DataAddress dataAddress)
        {
            var client = new WebClient();
            var address = dataAddress.Address.ToString();
            var resource = client.DownloadString(address);
            RawData rawData = new ServiceRawData(resource);

            return rawData;
        }
    }
}