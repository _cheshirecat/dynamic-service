﻿using DynamicService.Configuration.PropertyNaming.BenchmarkData;

namespace DynamicService.Configuration
{
    using PropertyNaming;
    using System.Collections.Generic;

    internal static class InternalSettings
    {
        internal static List<PropertyNamingRule> PropertyNamingRules
        {
            get
            {
                var list = new List<PropertyNamingRule>
                {
                    new PropertyNamingRule(new NameParts("default", "DS"), WhatDoWithParts.Append),
                    new PropertyNamingRule(new NameParts("1", "DS"), WhatDoWithParts.ToBegining),
                    new PropertyNamingRule(new NameParts("2", "DS"), WhatDoWithParts.ToBegining),
                    new PropertyNamingRule(new NameParts("3", "DS"), WhatDoWithParts.ToBegining),
                    new PropertyNamingRule(new NameParts("4", "DS"), WhatDoWithParts.ToBegining),
                    new PropertyNamingRule(new NameParts("5", "DS"), WhatDoWithParts.ToBegining),
                    new PropertyNamingRule(new NameParts("6", "DS"), WhatDoWithParts.ToBegining),
                    new PropertyNamingRule(new NameParts("7", "DS"), WhatDoWithParts.ToBegining),
                    new PropertyNamingRule(new NameParts("8", "DS"), WhatDoWithParts.ToBegining),
                    new PropertyNamingRule(new NameParts("9", "DS"), WhatDoWithParts.ToBegining),
                    new PropertyNamingRule(new NameParts("0", "DS"), WhatDoWithParts.ToBegining),

                };

                return list;
            }
        }
    }
}