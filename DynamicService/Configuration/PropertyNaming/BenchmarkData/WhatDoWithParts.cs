namespace DynamicService.Configuration.PropertyNaming.BenchmarkData
{
    public enum WhatDoWithParts
    {
        Append,
        ToBegining,
        ReplaceWith,
        Regex
    }
}