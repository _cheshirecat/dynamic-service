namespace DynamicService.Configuration.PropertyNaming.BenchmarkData
{
    public class NameParts
    {
        public NameParts(string propertyName, string customPart)
        {
            PropertyName = propertyName;
            CustomPart = customPart;
        }

        internal string PropertyName { get; set; }

        internal string CustomPart { get; set; }
    }
}