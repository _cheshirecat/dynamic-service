namespace DynamicService.Configuration.PropertyNaming
{
    using BenchmarkData;
    using Shapers;

    internal class NameShaperFactory
    {
        public static Shaper Shaper(WhatDoWithParts whatDoWithParts)
        {
            Shaper shaper = null;

            switch (whatDoWithParts)
            {
                case WhatDoWithParts.Append:
                    shaper = new Append();
                    break;
                case WhatDoWithParts.ToBegining:
                    shaper = new ToBegining();
                    break;
                case WhatDoWithParts.ReplaceWith:
                    shaper = new ReplaceWith();
                    break;
                case WhatDoWithParts.Regex:
                    break;
            }

            return shaper;
        }
    }
}