namespace DynamicService.Configuration.PropertyNaming
{
    internal class PropertyValidName
    {
        internal static string GatherValidName(PropertyNamingRule rule)
        {
            var shaper = NameShaperFactory.Shaper(rule.WhatDoWithParts);
            return shaper.ShapeParts(rule.NameParts);
        }
    }
}