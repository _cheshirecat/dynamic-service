﻿namespace DynamicService.Configuration.PropertyNaming.Shapers
{
    using BenchmarkData;

    internal abstract class Shaper
    {
        internal abstract string ShapeParts(NameParts parts);
    }
}