﻿namespace DynamicService.Configuration.PropertyNaming.Shapers
{
    using BenchmarkData;

    internal class ReplaceWith : Shaper
    {
        internal override string ShapeParts(NameParts parts)
        {
            return parts.CustomPart;
        }
    }
}