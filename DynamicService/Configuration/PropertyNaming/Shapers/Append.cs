namespace DynamicService.Configuration.PropertyNaming.Shapers
{
    using BenchmarkData;

    internal class Append : Shaper
    {
        internal override string ShapeParts(NameParts parts)
        {
            return string.Concat(parts.PropertyName, parts.CustomPart);
        }
    }
}