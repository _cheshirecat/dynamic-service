﻿namespace DynamicService.Configuration.PropertyNaming.Shapers
{
    using BenchmarkData;

    internal class ToBegining : Shaper
    {
        internal override string ShapeParts(NameParts parts)
        {
            return string.Concat(parts.CustomPart, parts.PropertyName);
        }
    }
}