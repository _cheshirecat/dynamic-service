﻿namespace DynamicService.Configuration.PropertyNaming
{
    using BenchmarkData;

    public class PropertyNamingRule
    {
        public PropertyNamingRule(NameParts parts, WhatDoWithParts whatDoWithParts)
        {
            NameParts = parts;
            WhatDoWithParts = whatDoWithParts;
        }

        internal NameParts NameParts { get; set; }

        internal WhatDoWithParts WhatDoWithParts { get; set; }
    }
}