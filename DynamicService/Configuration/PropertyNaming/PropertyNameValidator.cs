﻿namespace DynamicService.Configuration.PropertyNaming
{
    using System.Collections.Generic;

    internal static class PropertyNameValidator
    {
        internal static void BrushPropertyName(List<PropertyNamingRule> rules, ref string propertyName)
        {
            foreach (var rule in rules)
            {
                if (rule.NameParts.PropertyName == propertyName)
                {
                    propertyName = PropertyValidName.GatherValidName(rule);
                    break;
                }
            }
        }
    }
}