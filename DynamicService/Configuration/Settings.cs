﻿using System.Collections.Generic;
using DynamicService.Configuration.PropertyNaming;

namespace DynamicService.Configuration
{
    public class Settings
    {
        public Settings(string dataAddress, List<PropertyNamingRule> rules = null)
        {
            DataAddress = dataAddress;
            PropertyNamingRule = (rules == null)
                ? InternalSettings.PropertyNamingRules
                : ShapeList(rules);
        }

        public string DataAddress { get; set; }

        public List<PropertyNamingRule> PropertyNamingRule { get; set; }

        private List<PropertyNamingRule> ShapeList(List<PropertyNamingRule> rules)
        {
            rules.AddRange(InternalSettings.PropertyNamingRules);
            return rules;
        }
    }
}